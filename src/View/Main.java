package View;
import javax.swing.JFrame;

import Controller.Gui;
import Model.DataSeatPrice;
import Model.Seat;
import Model.TheaterManagement;
public class Main {
	public Main(Gui frameTabel){
		frameTabel.setSize(940, 600);
		frameTabel.setLocation(210, 80);
		frameTabel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameTabel.setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataSeatPrice dataSeat = new DataSeatPrice();
		Seat seat = new Seat(dataSeat);
		TheaterManagement manage = new TheaterManagement(seat.getSeatPrice());
		Gui gui = new Gui(manage);
		new Main(gui);
	}
}