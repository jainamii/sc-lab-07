package Controller;
import Model.TheaterManagement;

public class Controller {
	TheaterManagement manage;
	public Controller(TheaterManagement manage){
		this.manage = manage;
	}
	
	public void setSelectPrice(int price){
		manage.setSelectPrice(price);
	}
	
	public int[][] getTicketPrices(){
		return manage.getTicketPrices();
	}
	public int getSeatPrice(int row, int colum){
		return manage.getSeatPrice(row, colum);
	}
}
