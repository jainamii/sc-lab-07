package Controller;

import javax.crypto.spec.IvParameterSpec;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.table.*;

import Model.TheaterManagement;

public class Gui extends JFrame {
	JPanel panel = new JPanel();
	DefaultTableModel dataModel = new DefaultTableModel();
	JTable table = new JTable(dataModel);
	TheaterManagement manage;

	public Gui (TheaterManagement t) {
		this.manage = t;
		String[] columnNames = {"","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"};
		Object[][] data = {{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}};
		final int[] RowNum = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
		for (int col = 0; col < columnNames.length; col++){
			dataModel.addColumn(columnNames[col]);
		}

		for (int row = 0; row < 15; row++) {
			dataModel.addRow(data[row]);
		}
		for(int i = 0; i < 15; i++){
			for(int j = 1; j < 21; j++){
				dataModel.setValueAt(manage.getTicketPrices()[i][j], i, j);
				dataModel.setValueAt(RowNum[i],i,0);
			}
		}
		table.setOpaque(true);
		table.setFillsViewportHeight(true);
		table.setBackground(Color.white);
		table.getColumnModel().getColumn(0).setCellRenderer(new RowHeaderRenderer());
		//Create the scroll pane and add the table to it.
		final JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 60, 900, 263);

		//Total Price
		final JLabel label4 = new JLabel("�Ҥ���� :  "+manage.getTotal());
		label4.setBounds(200, 500, 160, 20);
		label4.setFont(new Font("Serif",Font.BOLD,20));

		// Row
		JLabel label1 = new JLabel("Row :");
		label1.setBounds(120, 370, 100, 20);
		label1.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField field1 = new JTextField();
		field1.setBounds(180, 366, 100, 30);
		field1.setFont(new Font("Serif",Font.BOLD,20));

		// Colum
		JLabel label2 = new JLabel("Colum :");
		label2.setBounds(370, 370, 100, 20);
		label2.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField field2 = new JTextField();
		field2.setBounds(450, 366, 100, 30);
		field2.setFont(new Font("Serif",Font.BOLD,20));

		//Search
		JLabel label3 = new JLabel("���Ҵ����Ҥ� :");
		label3.setBounds(120, 430, 160, 20);
		label3.setFont(new Font("Serif",Font.BOLD,20));
		final JTextField field3 = new JTextField("0");
		field3.setBounds(300, 426, 100, 30);
		field3.setFont(new Font("Serif",Font.BOLD,20));
		JLabel label5 = new JLabel("**��� 0 = ���Ҥҷ����� ���� ����Ҥ� ����ͧ��ä���**");
		label5.setBounds(200, 460, 300, 20);
		label5.setFont(new Font("Serif",Font.BOLD,10));
		
		//Buy Button
		JButton button1 = new JButton("Buy");
		button1.setBounds(620, 365, 250, 125);
		button1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg1) {
				// TODO Auto-generated method stub
				manage.getSeatPrice(Integer.valueOf(field1.getText()), Integer.valueOf(field2.getText()));
				manage.setSelectPrice(Integer.valueOf(field3.getText()));
				label4.setText("�Ҥ���� :  "+manage.getTotal());
				for(int i = 0; i < 15; i++){
					for(int j = 1; j < 21; j++){
						dataModel.setValueAt(manage.getTicketPrices()[i][j], i, j);
					}
				}
			}
		});

		JButton button2 = new JButton("����");
		button2.setBounds(450, 426, 100, 30);
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				manage.setSelectPrice(Integer.valueOf(field3.getText()));
				for(int i = 0; i < 15; i++){
					for(int j = 1; j < 21; j++){
						dataModel.setValueAt(manage.getTicketPrices()[i][j], i, j);
					}
				}
			}
		});

		//Add to panel.
		panel.add(scrollPane);
		panel.add(label1);
		panel.add(field1);
		panel.add(label2);
		panel.add(field2);
		panel.add(label3);
		panel.add(field3);
		panel.add(label4);
		panel.add(label5);
		panel.add(button1);
		panel.add(button2);
		panel.setLayout(null);
		getContentPane().add(panel);
	}


	static class RowHeaderRenderer extends DefaultTableCellRenderer {
		public RowHeaderRenderer() {
			setHorizontalAlignment(JLabel.CENTER);
		}

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if (table != null) {
				JTableHeader header = table.getTableHeader();

				if (header != null) {
					setForeground(header.getForeground());
					setBackground(header.getBackground());
					setFont(header.getFont());
				}
			}

			if (isSelected) {
				setFont(getFont().deriveFont(Font.BOLD));
			}

			setValue(value);
			return this;
		}
	}
} 